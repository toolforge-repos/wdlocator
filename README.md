Wikidata Locator
================

https://wdlocator.toolforge.org

This tool displays a map of OpenStreetMap features that are linked to Wikidata,
with a sidebar that shows details of any selected feature.

* Example: https://wdlocator.toolforge.org/#map=19/-32.05610/115.74098
* Source code: https://gitlab.wikimedia.org/toolforge-repos/wdlocator
* Issue tracker: https://phabricator.wikimedia.org/tag/tool-wdlocator
* Licence: GPL-3.0+
