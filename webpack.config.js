( function () {
	var Encore = require( '@symfony/webpack-encore' );

	Encore

		// Directory where compiled assets will be stored.
		.setOutputPath( './public/assets/' )

		// Public URL path used by the web server to access the output path.
		.setPublicPath( 'assets/' )

		// Main asset entry.
		.addEntry( 'app', [
			'./assets/app.js',
			'./assets/app.less'
		] )

		// Other options.
		.enableLessLoader()
		.cleanupOutputBeforeBuild()
		.disableSingleRuntimeChunk()
		.enableSourceMaps( !Encore.isProduction() )
		.enableVersioning( Encore.isProduction() );

	// eslint-disable-next-line no-undef
	module.exports = Encore.getWebpackConfig();

}() );
