import L from 'leaflet';
import 'leaflet-control-geocoder';
import 'leaflet/dist/leaflet.css';
import 'leaflet-control-geocoder/dist/Control.Geocoder.css';
import 'leaflet.locatecontrol';
import 'leaflet.locatecontrol/dist/L.Control.Locate.min.css';
import { MapState } from './MapState.js';
import { WdLocatorFeature } from './WdLocatorFeature.js';

const mapState = new MapState();

const loading = document.getElementById( 'loading' );
const pleaseZoom = document.getElementById( 'please-zoom' );
let selectedFeatures = [];
pleaseZoom.style.display = 'none';

let bboxes = {};
let loadedFeatures = {};
let featuresByItem = {};
let pendingCount = 0;
let refresh = false;

const legend = document.getElementById( 'map-legend' );
const showLegend = document.getElementById( 'show-legend' );
const hideLegend = document.getElementById( 'hide-legend' );
showLegend.addEventListener( 'click', () => {
	showLegend.style.display = 'none';
	hideLegend.style.display = 'block';
	legend.style.display = 'block';
	mapState.setLegend( true );
} );
hideLegend.addEventListener( 'click', () => {
	showLegend.style.display = 'block';
	hideLegend.style.display = 'none';
	legend.style.display = 'none';
	mapState.setLegend( false );
} );
legend.style.display = mapState.getLegend() ? 'block' : 'none';
showLegend.style.display = mapState.getLegend() ? 'none' : 'block';
hideLegend.style.display = mapState.getLegend() ? 'block' : 'none';

const map = L.map( 'map', { worldCopyJump: true } ).setView( [ mapState.getLat(), mapState.getLng() ], mapState.getZoom() );

document.querySelector( '#refresh a' )
	.addEventListener( 'click', () => {
		bboxes = {};
		loadedFeatures = {};
		featuresByItem = {};
		map.eachLayer( ( layer ) => {
			if ( layer instanceof L.Marker || layer instanceof L.Path ) {
				layer.remove();
			}
		} );
		refresh = true;
		loadMapFeatures( true );
		refresh = false;
	} );

// Tiles' docs: https://www.mediawiki.org/wiki/Wikimedia_Maps/API
L.tileLayer( 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png?lang=' + document.documentElement.lang, {
	maxZoom: 19,
	attribution: '<a href="http://www.openstreetmap.org/copyright">&copy; OSM contributors</a>'
} ).addTo( map );
L.Control.geocoder( {
	position: 'topleft',
	expand: 'click',
	defaultMarkGeocode: false,
	geocoder: L.Control.Geocoder.nominatim(),
	placeholder: App.i18n.searchPlaceholder,
	errorMessage: App.i18n.searchErrorMessage,
	iconLabel: App.i18n.searchIconLabel
} ).on( 'markgeocode', function ( e ) {
	map.setView( e.geocode.center, 11 );
} ).addTo( map );
L.control.locate( {
	strings: {
		title: App.i18n.locateTitle,
		metersUnit: App.i18n.locateMetersUnit,
		feetUnit: App.i18n.locateFeetUnit,
		popup: App.i18n.locatePopup,
		outsideMapBoundsMsg: App.i18n.locateOutsideMapBounds
	}
} ).addTo( map );
loadMapFeatures();
map.on( 'moveend', loadMapFeatures );
map.on( 'zoomend', loadMapFeatures );

/**
 * Divide a bounding box into multiple smaller bounding boxen.
 *
 * @param {string} largeBbox
 * @return {Array}
 */
function getBboxen( largeBbox ) {
	const parts = largeBbox.split( ',' );
	const latS = parts[ 0 ];
	const lonW = parts[ 1 ];
	const latN = parts[ 2 ];
	const lonE = parts[ 3 ];

	const bboxSize = App.bboxSize;
	const factor = 1 / bboxSize;
	const latMax = Math.ceil( latN * factor ) / factor;
	const latMin = Math.floor( latS * factor ) / factor;
	const lonMax = Math.ceil( lonE * factor ) / factor;
	const lonMin = Math.floor( lonW * factor ) / factor;

	const out = [];
	for ( let lat = latMin; lat <= latMax; lat += bboxSize ) {
		for ( let lon = lonMin; lon <= lonMax; lon += bboxSize ) {
			out.push( lat.toFixed( 2 ) + ',' + lon.toFixed( 2 ) );
		}
	}

	return out;
}

/**
 * The main move/zoom event handler, handles chopping the map up into smaller
 * areas and requesting data for each one.
 */
function loadMapFeatures() {
	// Store new map state.
	mapState.setZoom( map.getZoom() );
	mapState.setLat( map.getCenter().wrap().lat );
	mapState.setLng( map.getCenter().wrap().lng );

	// Do nothing else if we're not zoomed in far enough.
	if ( map.getZoom() < 16 ) {
		pleaseZoom.style.display = 'block';
		return;
	}
	pleaseZoom.style.display = 'none';

	// Bounding boxes.
	// S lat, W long, N lat, E long
	const largeBbox = map.getBounds().getSouthWest().wrap().lat.toFixed( 5 ) +
		',' + map.getBounds().getSouthWest().wrap().lng.toFixed( 5 ) +
		',' + map.getBounds().getNorthEast().wrap().lat.toFixed( 5 ) +
		',' + map.getBounds().getNorthEast().wrap().lng.toFixed( 5 );

	getBboxen( largeBbox ).forEach( ( bbox ) => {
		if ( bboxes[ bbox ] !== undefined ) {
			// Already fetched.
			return;
		}
		bboxes[ bbox ] = true;
		pushPending();
		fetch( '/api/data/' + bbox + ( refresh ? '?refresh' : '' ) )
			.then( ( response ) => response.text() )
			.then( ( str ) => JSON.parse( str ) )
			.then( handleQueryResponse )
			.finally( popPending );
	} );
}

/**
 * Handles the API response for a single square of the map.
 *
 * @param {Object} data The returned data, with keys `features`, `entities`,
 * `entitiesWithoutOsm`, and `files`.
 */
function handleQueryResponse( data ) {
	// OSM features.
	Object.keys( data.features ).forEach( ( featureId ) => {
		if ( loadedFeatures[ featureId ] !== undefined ) {
			// Already added to map.
			return;
		}
		loadedFeatures[ featureId ] = true;
		const feature = data.features[ featureId ];
		const hasImage = feature.wikidata
			.filter( ( wdId ) => data.entities[ wdId ].image !== null )
			.length === feature.wikidata.length;
		const mapFeature = new WdLocatorFeature( 'osm', feature.geom, { image: hasImage } );
		feature.wikidata.forEach( ( wikidataId ) => {
			mapFeature.addWikidataId( wikidataId );
			if ( featuresByItem[ wikidataId ] === undefined ) {
				featuresByItem[ wikidataId ] = [];
			}
			featuresByItem[ wikidataId ].push( mapFeature );
		} );
		mapFeature.addTo( map );
		mapFeature.setClickHandler( clickHandler );
	} );

	// Wikidata features without OSM geometry.
	data.entitiesWithoutOsm.forEach( ( entityInfo ) => {
		if ( featuresByItem[ entityInfo.id ] !== undefined ) {
			// This Wikidata item already has a feature.
			return;
		}
		const mapFeature = new WdLocatorFeature( 'nonosm', [ entityInfo.lat, entityInfo.lon ], entityInfo );
		mapFeature.addWikidataId( entityInfo.id );
		mapFeature.addTo( map );
		mapFeature.setClickHandler( clickHandler );
		featuresByItem[ entityInfo.id ] = [ mapFeature ];
	} );

	// Commons files.
	data.files.forEach( ( file ) => {
		const fileFeature = new WdLocatorFeature( 'file', [ file.lat, file.lon ], file );
		fileFeature.addTo( map );
		fileFeature.setClickHandler( clickHandler );
	} );
}

function clickHandler( event ) {
	const newSelection = event.target.wdLocatorFeature;
	selectedFeatures.map( ( f ) => f.unselect() );
	// Select the current feature.
	newSelection.select();
	selectedFeatures = [ newSelection ];
	// Also select all others with any matching Wikidata ID.
	newSelection.getWikidataIds().forEach( ( wikidataId ) => {
		featuresByItem[ wikidataId ].forEach( ( f ) => {
			f.select();
			selectedFeatures.push( f );
		} );
	} );
	const panels = {};
	newSelection.getWikidataIds().forEach( ( wikidataId ) => {
		panels[ wikidataId ] = 'wikidata';
	} );
	// @todo Handle multiple iamges within the clicky radius.
	if ( newSelection.getFilename() ) {
		panels[ newSelection.getFilename() ] = 'commons';
	}
	popover( panels );
}

/**
 * @param {Object} panelsInfo
 */
function popover( panelsInfo ) {
	const panel = document.getElementById( 'panel' );
	panel.innerHTML = '';
	Object.keys( panelsInfo ).forEach( ( id ) => {
		pushPending();
		fetch( '/api/' + panelsInfo[ id ] + '/' + id )
			.then( ( response ) => response.text() )
			.then( ( html ) => {
				panel.innerHTML += html;
				panel.querySelectorAll( '.item-close' ).forEach( ( closeButton ) => {
					closeButton.addEventListener( 'click', ( event ) => {
						event.target.parentNode.remove();
					} );
				} );
				panel.scrollIntoView( { behavior: 'smooth', block: 'start' } );
			} )
			.finally( popPending );
	} );
}

function pushPending() {
	pendingCount++;
	loading.classList.add( 'active' );
}

function popPending() {
	pendingCount--;
	if ( pendingCount <= 0 ) {
		pendingCount = 0;
		loading.classList.remove( 'active' );
	}
}
