class MapState {

	#lat = 0.0;

	#lng = 0.0;

	#zoom = 3;

	#legend = false;

	#localStorageName = 'wdlocator';

	constructor() {
		const localStorageVal = window.localStorage.getItem( this.#localStorageName );
		const localStorageData = localStorageVal ? JSON.parse( localStorageVal ) : {};
		const hashParts = window.location.hash.slice( 1 ).split( '=' );
		if ( hashParts[ 0 ] === 'map' ) {
			const hashMapParts = hashParts[ 1 ].split( '/' );
			this.#zoom = hashMapParts[ 0 ];
			this.#lat = hashMapParts[ 1 ];
			this.#lng = hashMapParts[ 2 ];
		} else {
			const zoom = parseInt( localStorageData.zoom );
			if ( zoom ) {
				this.#zoom = zoom;
			}
			const lat = parseFloat( localStorageData.lat );
			if ( lat ) {
				this.#lat = lat;
			}
			const lng = parseFloat( localStorageData.lng );
			if ( lng ) {
				this.#lng = lng;
			}
		}
		if ( localStorageData.legend !== undefined ) {
			this.#legend = localStorageData.legend;
		}
	}

	getLat() {
		return this.#lat;
	}

	setLat( lat ) {
		this.#lat = lat.toFixed( 5 );
		this.#update();
	}

	getLng() {
		return this.#lng;
	}

	setLng( lng ) {
		this.#lng = lng.toFixed( 5 );
		this.#update();
	}

	getZoom() {
		return this.#zoom;
	}

	setZoom( zoom ) {
		this.#zoom = zoom;
		this.#update();
	}

	getLegend() {
		return this.#legend;
	}

	setLegend( legend ) {
		this.#legend = legend;
		this.#update();
	}

	#update() {
		window.location.hash = '#map=' +
			this.getZoom() +
			'/' + this.getLat() +
			'/' + this.getLng();
		window.localStorage.setItem( this.#localStorageName, JSON.stringify( {
			lat: this.getLat(),
			lng: this.getLng(),
			zoom: this.getZoom(),
			legend: this.#legend
		} ) );
	}
}

export { MapState };
