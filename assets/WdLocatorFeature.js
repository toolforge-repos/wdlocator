import L from 'leaflet';

class WdLocatorFeature {

	#feature = null;

	#featureClickable = null;

	#wikidataIds = [];

	#filename = null;

	constructor( type, geom, info ) {
		if ( type === 'file' ) {
			this.#feature = L.circleMarker( geom, {
				radius: 2,
				opacity: 0.6,
				stroke: 2,
				color: '#33acff'
			} );
			this.#featureClickable = L.circleMarker( geom, {
				stroke: 8,
				color: 'transparent'
			} );
			if ( info.title ) {
				this.#filename = info.title;
			}
		}

		if ( type === 'nonosm' ) {
			this.#feature = L.circleMarker( geom, {
				radius: 3,
				stroke: 2,
				opacity: 0.8,
				fillColor: 'red'
			} );
			if ( info.image ) {
				this.#feature.setStyle( { color: '#2DC800' } );
			} else {
				this.#feature.setStyle( { color: '#FF4848' } );
			}
			this.#featureClickable = L.circleMarker( geom, {
				stroke: 8,
				color: 'transparent'
			} );
		}

		if ( type === 'osm' ) {
			const styleUnselected = {
				radius: 3,
				weight: 3,
				opacity: 0.5,
				fill: false
			};
			if ( geom.length === 1 ) {
				this.#feature = L.circleMarker( geom[ 0 ], styleUnselected );
				this.#featureClickable = L.circleMarker( geom[ 0 ], { weight: 12, color: 'transparent' } );
			} else {
				this.#feature = L.polyline( geom, styleUnselected );
				this.#featureClickable = L.polyline( geom, { weight: 14, color: 'transparent' } );
			}
			if ( info.image ) {
				this.#feature.setStyle( { color: '#2DC800' } );
			} else {
				this.#feature.setStyle( { color: '#FF4848' } );
			}
		}

		// Link the feature back to this object.
		this.#feature.wdLocatorFeature = this;

		// Make all clicks on the larger clickable feature the same as clicking the actual feature.
		this.#featureClickable.on( 'click', () => {
			this.#feature.fire( 'click' );
		} );
	}

	select() {
		this.#featureClickable.setStyle( { color: 'yellow', opacity: 0.3 } );
	}

	unselect() {
		this.#featureClickable.setStyle( { color: 'transparent', opacity: 1 } );
	}

	addWikidataId( id ) {
		this.#wikidataIds.push( id );
	}

	getWikidataIds() {
		return this.#wikidataIds;
	}

	getFilename() {
		return this.#filename;
	}

	setClickHandler( handler ) {
		this.#feature.on( 'click', handler );
	}

	addTo( map ) {
		this.#feature.addTo( map );
		this.#featureClickable.addTo( map );
	}
}

export { WdLocatorFeature };
