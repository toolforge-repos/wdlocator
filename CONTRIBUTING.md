# Contributing to Wikidata Locator

To test Redis configuration locally, set `APP_ENV` to `prod`
and open an SSH tunnel to Toolforge's Redis server:

```console
$ ssh -N -L 6379:redis.svc.tools.eqiad1.wikimedia.cloud:6379
```
