<?php

namespace App;

use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Wikidata {

	private const PROP_IMAGE = 'P18';

	private const PROP_COMMONS_CAT = 'P373';

	private const PROP_TOPIC_CAT = 'P910';

	/** @var CacheItemPoolInterface */
	private $cache;

	/** @var HttpClientInterface */
	private $httpClient;

	public function __construct( CacheItemPoolInterface $cache, HttpClientInterface $httpClient ) {
		$this->cache = $cache;
		$this->httpClient = $httpClient;
	}

	private function getCacheItem( string $id ): CacheItemInterface {
		$cacheVersion = 2;
		return $this->cache->getItem( 'wikidata' . $cacheVersion . $id );
	}

	public function get( array $ids, bool $refresh = false ): array {
		$items = [];
		$idsToFetch = [];
		foreach ( $ids as $id ) {
			$cachItem = $this->getCacheItem( $id );
			if ( $cachItem->isHit() && !$refresh ) {
				$item = $cachItem->get();
				$items[$id] = $item;
			} elseif ( !in_array( $id, $idsToFetch ) ) {
				$idsToFetch[] = $id;
			}
		}

		if ( empty( $idsToFetch ) ) {
			return $items;
		}

		$batchSize = 50;
		foreach ( array_chunk( $idsToFetch, $batchSize ) as $idsChunk ) {
			$items = array_merge( $items, $this->getBatch( $idsChunk ) );
		}
		return $items;
	}

	/**
	 * Get and cache a batch of Wikidata entities.
	 */
	private function getBatch( array $ids ): array {
		$getEntitiesUrl = 'https://www.wikidata.org/w/api.php?' . http_build_query( [
			'action' => 'wbgetentities',
			'format' => 'json',
			'ids' => implode( '|', $ids ),
			'formatversion' => 2,
		] );
		$response = $this->httpClient->request( 'GET', $getEntitiesUrl );
		$json = $response->getContent();
		$entitiesData = json_decode( $json, true );
		$items = [];
		foreach ( $entitiesData['entities'] ?? [] as $id => $itemData ) {
			$commonsCat = null;
			if ( isset( $itemData['claims'][self::PROP_COMMONS_CAT] ) ) {
				$commonsCat = $itemData['claims'][self::PROP_COMMONS_CAT][0]['mainsnak']['datavalue']['value'];
			}
			if ( !$commonsCat && isset( $itemData['claims'][self::PROP_TOPIC_CAT] ) ) {
				$topicCatId = $itemData['claims'][self::PROP_TOPIC_CAT][0]['mainsnak']['datavalue']['value']['id'];
				$topicItems = $this->get( [ $topicCatId ] );
				if ( $topicItems ) {
					$topicItem = reset( $topicItems );
					$commonsCat = $topicItem['commons_cat'];
				}
			}
			// @todo don't include unnecessary info here.
			$item = [
				// The OSM Wikidata ID might be a redirect to the actual one, so return both.
				'id' => $itemData['id'],
				'id_orig' => $id,
				'is_redirect' => $id !== $itemData['id'],
				'labels' => $itemData['labels'],
				'descriptions' => $itemData['descriptions'],
				'commons_cat' => $commonsCat,
				'image' => $itemData['claims'][self::PROP_IMAGE][0]['mainsnak']['datavalue']['value'] ?? null,
			];
			$items[$id] = $item;
			$cachItem = $this->getCacheItem( $id );
			$cachItem->set( $item );
			$this->cache->save( $cachItem );
		}
		return $items;
	}

	/**
	 */
	public function getWithinBoundingBox( array $bbox, bool $refresh = false ): array {
		$cacheKey = 'wdqs-bbox' . $bbox['latS'] . $bbox['lonW'];
		$cacheItem = $this->cache->getItem( $cacheKey );
		if ( $cacheItem->isHit() && !$refresh ) {
			return $cacheItem->get();
		}
		$query = 'SELECT ?item ?lat ?lon ?image WHERE {
			SERVICE wikibase:box {
				?item wdt:P625 ?coords
					. bd:serviceParam wikibase:cornerSouthWest
						"Point(' . $bbox['lonW'] . ' ' . $bbox['latS'] . ')"^^geo:wktLiteral
					. bd:serviceParam wikibase:cornerNorthEast
						"Point(' . $bbox['lonE'] . ' ' . $bbox['latN'] . ')"^^geo:wktLiteral
			}
			?item p:P625 ?statement .
			?statement psv:P625 ?node .
			?node wikibase:geoLatitude ?lat .
			?node wikibase:geoLongitude ?lon .
			MINUS { ?item wdt:P576 ?demolished }
			OPTIONAL { ?item wdt:P18 ?image }
		} LIMIT 1000';
		$url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql?format=json&query=' . urlencode( $query );
		$response = $this->httpClient->request( 'GET', $url );
		$json = $response->getContent();
		$data = json_decode( $json, true );
		$out = [];
		foreach ( $data['results']['bindings'] ?? [] as $result ) {
			$out[] = [
				'id' => substr( $result['item']['value'], strlen( 'http://www.wikidata.org/entity/' ) ),
				'lat' => $result['lat']['value'],
				'lon' => $result['lon']['value'],
				'image' => $result['image']['value'] ?? null,
			];
		}
		$cacheItem->set( $out );
		$this->cache->save( $cacheItem );
		return $out;
	}
}
