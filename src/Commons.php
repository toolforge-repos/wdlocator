<?php

namespace App;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Commons {

	/** @var CacheItemPoolInterface */
	private $cache;

	/** @var HttpClientInterface */
	private $httpClient;

	private string $apiurl = 'https://commons.wikimedia.org/w/api.php';

	public function __construct( CacheItemPoolInterface $cache, HttpClientInterface $httpClient ) {
		$this->cache = $cache;
		$this->httpClient = $httpClient;
	}

	/**
	 * Get and cache details of files within a given bounding box.
	 */
	public function getWithinBoundingBox( array $bbox, bool $refresh = false ): array {
		$params = [
			'action' => 'query',
			'list' => 'geosearch',
			'gsbbox' => sprintf( '%s|%s|%s|%s', $bbox['latN'], $bbox['lonW'], $bbox['latS'], $bbox['lonE'] ),
			'gsnamespace' => '6',
			'gslimit' => '500',
			'format' => 'json',
		];
		$url = $this->apiurl . '?' . http_build_query( $params );
		$cacheItem = $this->cache->getItem( 'commons' . md5( $url ) );
		if ( $cacheItem->isHit() && !$refresh ) {
			return $cacheItem->get();
		}
		$response = $this->httpClient->request( 'GET', $url );
		$json = $response->getContent();
		$data = json_decode( $json, true );
		$out = [];
		foreach ( $data['query']['geosearch'] ?? [] as $file ) {
			$out[] = [
				// `title` has 'File:' prefix.
				'title' => $file['title'],
				'lat' => $file['lat'],
				'lon' => $file['lon'],
			];
		}
		$cacheItem->set( $out );
		$this->cache->save( $cacheItem );
		return $out;
	}

	public function getInfo( string $filename ): array {
		$params = [
			'action' => 'query',
			'prop' => 'imageinfo',
			'iiprop' => 'url|extmetadata',
			// 320 is one of Commons' pre-rendered sizes: https://gerrit.wikimedia.org/g/operations/mediawiki-config/+/a00091f31c10f819d7e0b1a98010a2219f466e68/wmf-config/InitialiseSettings.php#11005
			'iiurlwidth' => '320',
			'format' => 'json',
			'formatversion' => 2,
			'titles' => $filename,
		];
		$url = $this->apiurl . '?' . http_build_query( $params );
		$cacheItem = $this->cache->getItem( 'commons' . md5( $url ) );
		if ( $cacheItem->isHit() ) {
			return $cacheItem->get();
		}
		$response = $this->httpClient->request( 'GET', $url );
		$json = $response->getContent();
		$data = json_decode( $json, true );
		if ( !$data ) {
			return null;
		}
		$out = $data['query']['pages'][0];
		$cacheItem->set( $out );
		$this->cache->save( $cacheItem );
		return $out;
	}
}
