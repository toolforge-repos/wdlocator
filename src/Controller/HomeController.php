<?php

// phpcs:disable MediaWiki.Commenting.FunctionAnnotations.UnrecognizedAnnotation

namespace App\Controller;

use App\Commons;
use App\Wikidata;
use DateInterval;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HomeController extends AbstractController {

	private const BBOX_SIZE = 0.01;

	/**
	 * @return Response
	 */
	#[Route( "/", name:"home" ) ]
	public function index(): Response {
		return $this->render( 'base.html.twig', [
			'bbox_size' => self::BBOX_SIZE,
		] );
	}

	/**
	 * @param array $element
	 * @param string $wikidata
	 * @param array &$out
	 */
	private function transformElement( array $element, string $wikidata, array &$out ) {
		$elementId = $element['id'];
		$wikidataIds = array_map( 'trim', explode( ';', $wikidata ) );

		// If we've seen this feature before, just add the Wikidata IDs.
		if ( isset( $out[$elementId] ) ) {
			$out[$elementId]['wikidata'] = array_unique( array_merge( $out[$elementId]['wikidata'], $wikidataIds ) );
			return;
		}

		// Ignore relations without members.
		if ( $element['type'] === 'relation' && !isset( $element['members'] ) ) {
			return;
		}

		// Relations.
		if ( isset( $element['members'] ) ) {
			foreach ( $element['members'] as $member ) {
				$member['id'] = $member['ref'];
				$this->transformElement( $member, $element['tags']['wikidata'], $out );
			}
			return;
		}

		// Points are turned into lines with a single node.
		if ( $element['type'] === 'node' ) {
			$out[$elementId] = [
				'id' => $elementId,
				'wikidata' => $wikidataIds,
				'geom' => [ [ $element['lat'], $element['lon'] ] ],
			];
			return;
		}

		// Areas and lines are handled the same.
		$lineCoords = [];
		foreach ( $element['geometry'] as $geom ) {
			$lineCoords[] = [ $geom['lat'], $geom['lon'] ];
		}
		$leafletEl = [
			'id' => $elementId,
			'wikidata' => $wikidataIds,
			'geom' => $lineCoords,
		];
		$out[$elementId] = $leafletEl;
	}

	/**
	 * @return Response
	 */
	#[ Route( "/api/data/{lat},{lon}", name:"api_data" ) ]
	public function data(
		HttpClientInterface $httpClient, CacheItemPoolInterface $cache, Wikidata $wikidata, Commons $commons,
		Request $request, string $lat, string $lon
	) {
		$that = $this;
		$precision = strlen( self::BBOX_SIZE ) - 2;
		$bbox = [];
		$bbox['latS'] = round( $lat, $precision );
		$bbox['latN'] = $bbox['latS'] + self::BBOX_SIZE;
		$bbox['lonW'] = round( $lon, $precision );
		$bbox['lonE'] = $bbox['lonW'] + self::BBOX_SIZE;

		$cacheVersion = 2;
		$featuresCacheKey = 'overpass' . $cacheVersion . $bbox['latS'] . $bbox['lonW'];
		$cachedFeatures = $cache->getItem( $featuresCacheKey );
		$cachedFeatures->expiresAfter( new DateInterval( 'P1W' ) );
		$refresh = $request->get( 'refresh' ) !== null;
		if ( $cachedFeatures->isHit() && !$refresh ) {
			$features = $cachedFeatures->get();
		} else {
			$overpassUrl = 'https://overpass-api.de/api/interpreter?data=';
			// S lat, W long, N lat, E long
			$bboxString = sprintf( '%s,%s,%s,%s', $bbox['latS'], $bbox['lonW'], $bbox['latN'], $bbox['lonE'] );
			$query = '[out:json][timeout:25][bbox:' . $bboxString . '];
			(
				relation[wikidata][!boundary];
				way[wikidata];
			);
			out geom;';
			$response = $httpClient->request( 'GET', $overpassUrl . urlencode( $query ) );
			$json = $response->getContent();
			$data = json_decode( $json, true );

			$features = [];
			foreach ( $data['elements'] ?? [] as $element ) {
				$that->transformElement( $element, $element['tags']['wikidata'], $features );
			}

			$cachedFeatures->set( $features );
			$cache->save( $cachedFeatures );
		}

		$wikidataIds = [];
		foreach ( $features as $feature ) {
			$wikidataIds = array_merge( $wikidataIds, $feature['wikidata'] );
		}

		return new JsonResponse( [
			'features' => $features,
			'entities' => $wikidata->get( $wikidataIds, $refresh ),
			'entitiesWithoutOsm' => $wikidata->getWithinBoundingBox( $bbox, $refresh ),
			'files' => $commons->getWithinBoundingBox( $bbox, $refresh ),
		] );
	}

	/**
	 * @return Response
	 */
	#[ Route( "/api/wikidata/{qid}", name: "api_wikidata" ) ]
	public function wikidata(
		Wikidata $wikidata, HttpClientInterface $httpClient, CacheInterface $cache, string $qid
	) {
		$item = $wikidata->get( [ $qid ] )[$qid] ?? null;

		if ( $item['image'] ) {
			// Construct the image URL (to avoid an additional API call).
			$item['image'] = str_replace( ' ', '_', $item['image'] );
			$imageHash = md5( $item['image'] );
			$item['image_url'] = 'https://upload.wikimedia.org/wikipedia/commons/thumb/'
				. substr( $imageHash, 0, 1 ) . '/' . substr( $imageHash, 0, 2 )
				. '/' . urlencode( $item['image'] ) . '/300px-' . urlencode( $item['image'] );
		}

		$commonsCatInfo = null;
		if ( $item['commons_cat'] ) {
			$commonsCatTitle = $item['commons_cat'];
			$cacheKey = 'commons-catinfo' . $commonsCatTitle;
			$commonsCatInfo = $cache->get(
				$cacheKey,
				static function ( ItemInterface $item ) use ( $httpClient, $commonsCatTitle ) {
					$item->expiresAfter( new DateInterval( 'P1D' ) );
					$url = 'https://commons.wikimedia.org/w/api.php?' . http_build_query( [
						"action" => "query",
						"format" => "json",
						"prop" => "categoryinfo",
						"titles" => 'Category:' . $commonsCatTitle,
						"formatversion" => "2"
					] );
					$response = $httpClient->request( 'GET', $url );
					$json = $response->getContent();
					$data = json_decode( $json, true );
					if ( !isset( $data['query']['pages'] ) ) {
						return null;
					}
					return reset( $data['query']['pages'] );
				}
			);
		}

		return $this->render( 'panel_wikidata.html.twig', [
			'data' => $item,
			'commons_category' => $commonsCatInfo,
		] );
	}

	/**
	 * @return Response
	 */
	#[ Route( "/api/commons/{filename}", name: "api_commons" ) ]
	public function commons( Commons $commons, string $filename ) {
		return $this->render( 'panel_commons.html.twig', [
			'info' => $commons->getInfo( $filename ),
		] );
	}
}
