module.exports = {
    extends: 'wikimedia/client',
    env: {
        browser: true,
        node: true,
        es2022: true,
    },
    globals: {
        App: true
    },
    parserOptions: {
        ecmaVersion: 13,
        sourceType: 'module'
    },
    rules: {
        "es-x/no-class-fields": "off",
        "es-x/no-promise-prototype-finally": "off"
    }
};
